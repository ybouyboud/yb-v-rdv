package ma.bcp.digital.project.service.exceptions;

public class TypeCompteException extends RuntimeException {
    public TypeCompteException(String message) {
        super(message);
    }
}
