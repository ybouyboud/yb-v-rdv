package ma.bcp.digital.project.service;

import lombok.AllArgsConstructor;
import ma.bcp.digital.project.domain.entities.*;
import ma.bcp.digital.project.domain.repositories.RdvRepository;
import ma.bcp.digital.project.service.exceptions.RdvException;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@AllArgsConstructor
public class RdvService {

    private RdvRepository rdvRepository;
    private DossierService dossierService;
    private ConseillerService conseillerService;
    private DisponibiliteService disponibiliteService;

    public RdvService(RdvRepository rdvRepository) {
        this.rdvRepository = rdvRepository;
    }

    public List<Rdv> findAll(){
        if (rdvRepository.findAll().toString().equals("[]"))
            throw new RdvException("Pas de rdv(s) à afficher.");
        return (List<Rdv>) rdvRepository.findAll();
    }

    public Rdv createRdv(String dossierId, String date, String heureDebut){
        Conseiller conseiller = conseillerService.findConseillerUsingIdDossier(dossierId);
        this.verifierDisponibilites();
        Dossier dossier = dossierService.findDossierById(dossierId);

        String[] t = date.split("-");
        int day=Integer.parseInt(t[2]);
        int month=Integer.parseInt(t[1]);
        int year=Integer.parseInt(t[0]);
        LocalDate date1 = LocalDate.of(year,month,day);
        LocalTime time  = LocalTime.of(Integer.parseInt(heureDebut.substring(0,2)),Integer.parseInt(heureDebut.substring(3)));

        String heureFin = time.plusMinutes(40).toString();

        if(conseillerService.findCreneauByDateAndHeure(conseiller.getId().getValue(),date,heureDebut) == null)
          throw new RdvException("La date choisi : "+date+" avec la plage horaire de : "+heureDebut+" à "+heureFin+" n'est pas disponibile ");

        Rdv rdv = new Rdv(conseiller,dossier,date1,heureDebut,heureFin,"GMT+1");
        rdv.setId(new RdvId());
        try{
          rdvRepository.save(rdv);
          conseillerService.supprimerCrenauxConseillerByHeure(conseiller.getId().getValue(),date,heureDebut);

        }catch (Exception e){
          throw new RdvException("Impossible de persister le RDV dans la bdd.");
        }finally {
          return rdv;
        }

    }

    public Rdv updateRdv(String dossierId,String date, String heureDebut){
        Dossier dossier = dossierService.findDossierById(dossierId);
        Rdv rdv = rdvRepository.findRdvByDossier(dossier);
        rdvRepository.delete(rdv);
        return this.createRdv(dossierId,date,heureDebut);
    }

    public void verifierDisponibilites(){
        List<Conseiller> conseillers = conseillerService.findAll();
        for(Conseiller conseiller : conseillers){
            int sizeLst = this.outdatedDisponibility(conseiller,conseiller.getDisponibilites());
            while(sizeLst < 5){
                conseillerService.ajouterUneNouvelleDate(conseiller.getId().getValue());
                sizeLst++;
            }
        }
    }

    public int outdatedDisponibility(Conseiller conseiller, List<Disponibilite> disponibilites){
        LocalDate today = LocalDate.now();
        List<Disponibilite> disponibiliteList = new CopyOnWriteArrayList<>(disponibilites);
        for(Disponibilite disponibilite : disponibiliteList) {
            if ( today.isAfter(disponibilite.getDate())) {
                conseillerService.deleteDispoForCons(conseiller.getId().getValue(), disponibilite);
                disponibiliteList.remove(disponibilite);
                disponibiliteService.deleteDisponibilite(disponibilite);
            }
        }
        return disponibiliteList.size();
    }


}
