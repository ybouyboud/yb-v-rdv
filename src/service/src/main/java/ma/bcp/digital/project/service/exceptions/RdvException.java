package ma.bcp.digital.project.service.exceptions;

public class RdvException extends RuntimeException{
    public RdvException(String message) {
        super(message);
    }
}
