package ma.bcp.digital.project.service.exceptions;

public class ProspectException extends RuntimeException {
    public ProspectException(String message) {
        super(message);
    }
}
