package ma.bcp.digital.project.service.exceptions;

public class ProductException extends RuntimeException {
    public ProductException(String message) {
        super(message);
    }
}
