package ma.bcp.digital.project.service.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PieceIdentiteExceptionResponse {
    private String pieceIdentiteException;
}
