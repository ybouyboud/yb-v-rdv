package ma.bcp.digital.project.service.exceptions;

public class PieceJustifException extends RuntimeException {
    public PieceJustifException(String message) {
        super(message);
    }
}
