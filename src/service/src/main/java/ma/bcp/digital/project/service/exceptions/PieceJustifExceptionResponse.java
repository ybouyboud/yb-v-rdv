package ma.bcp.digital.project.service.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PieceJustifExceptionResponse {
    private String pieceJustifException;
}
