package ma.bcp.digital.project.service.exceptions;


public class ConseillerException extends RuntimeException {
    public ConseillerException(String message) {
        super(message);
    }
}
