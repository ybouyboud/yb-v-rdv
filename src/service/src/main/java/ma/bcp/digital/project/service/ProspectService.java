package ma.bcp.digital.project.service;

import lombok.AllArgsConstructor;
import ma.bcp.digital.project.domain.entities.*;
import ma.bcp.digital.project.domain.repositories.*;
import ma.bcp.digital.project.service.exceptions.AdresseException;
import ma.bcp.digital.project.service.exceptions.ProductException;
import ma.bcp.digital.project.service.exceptions.ProspectException;
import ma.bcp.digital.project.service.exceptions.TypeCompteException;

import java.util.List;

@AllArgsConstructor
public class ProspectService {

    private ProspectRepository prospects;
    private PieceIdentiteRepository pieceIdentiteRepository;
    private AdresseRepository adresseRepository;
    private ProductRepository productRepository;
    private TypeCompteRepository typeCompteRepository;


    public List<Prospect> findAll() {
        return (List<Prospect>) prospects.findAll();
    }

    public Prospect createOrUpdateProspect(Prospect p){

        try{
            List<PieceIdentite> pieceIdentiteList = p.getPiecesIdentite();
            this.saveAdresses(p.getAdresses());
            this.saveProducts(p.getProduits());
            this.saveTypeCompte(p.getTypeComptes());
            p.setPiecesIdentite(null);
            prospects.save(p);
            if(!pieceIdentiteList.isEmpty()){
                for(PieceIdentite pi : pieceIdentiteList){
                    pi.setProspect(p);
                    pieceIdentiteRepository.save(pi);
                }
            }
            p.setPiecesIdentite(pieceIdentiteList);
            return prospects.save(p);
        }
        catch(Exception e){
            throw new ProspectException("Impossible d'ajouter le prospect");
        }
    }

    public Prospect getProspectById(String cin) {
        return prospects.findProspectById(new PersonneId(cin));
    }

    public void saveAdresses(List<Adresse> adresses){
        try{
            if(!adresses.isEmpty()){
                for(Adresse adr : adresses){
                    adresseRepository.save(adr);
                }
            }
        }catch(Exception e){
            throw new AdresseException("Adresse déjà existante");
        }
    }

    public void saveProducts(List<Product> products){
        try{
            if(!products.isEmpty()){
                for(Product product : products){
                    productRepository.save(product);
                }
            }
        }catch(Exception e){
            throw new ProductException("Produit déjà existant");
        }
    }

    public void saveTypeCompte(List<TypeCompte> typeComptes){
        try{
            if(!typeComptes.isEmpty()){
                for(TypeCompte typeCompte : typeComptes){
                    typeCompteRepository.save(typeCompte);
                }
            }
        }catch(Exception e){
            throw new TypeCompteException("Problème save TC");
        }
    }

}
