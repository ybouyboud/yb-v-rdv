package ma.bcp.digital.project.service;

import ma.bcp.digital.project.domain.entities.User;
import ma.bcp.digital.project.domain.repositories.UserRepository;

import java.util.List;

public class UserService {

    private UserRepository users;

    public UserService(UserRepository users) {
        this.users = users;
    }

    public List<User> findAll() {
        return (List<User>) users.findAll();
    }

}
