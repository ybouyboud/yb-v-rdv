package ma.bcp.digital.project.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import ma.bcp.digital.project.domain.entities.*;
import ma.bcp.digital.project.domain.repositories.ConseillerRepository;
import ma.bcp.digital.project.service.exceptions.ConseillerException;

import java.io.FileInputStream;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@AllArgsConstructor
public class ConseillerService {

    private ConseillerRepository conseillers;
    private DisponibiliteService disponibiliteService;
    private DossierService dossierService;


    public ConseillerService(ConseillerRepository conseillers) {
        this.conseillers = conseillers;
    }

    public List<Conseiller> findAll() {
        if (conseillers.findAll().toString().equals("[]"))
            throw new ConseillerException("Pas de conseiller(s) à afficher.");
        return (List<Conseiller>) conseillers.findAll();
    }

    public Conseiller findConseillerById(String cin) {
        Conseiller conseiller = conseillers.findConseillerById(new PersonneId(cin));
        if (conseiller == null) throw new ConseillerException("Conseiller avec CIN " + cin + " introuvable");
        return conseiller;
    }

    public Conseiller findConseillerByMatriculeConseiller(String matriculeConseiller) {
        try {
            return conseillers.findConseillerByMatriculeConseiller(matriculeConseiller);
        } catch (Exception ex) {
            throw new ConseillerException("Conseiller avec MatriculeConseiller " + matriculeConseiller + " introuvable.");
        }
    }

    public Conseiller createOrUpdateConseiller(Conseiller conseiller) {

        Conseiller conseiller1 = this.findConsForCreate(conseiller);
        if (conseiller1 == null)
            throw new ConseillerException("Conseiller avec cin : " + conseiller.getId().getValue() + " déjà existant");
        try {
            return conseillers.save(conseiller1);
        } catch (Exception e) {
            throw new ConseillerException("Conseiller avec matriculeConseiller : " + conseiller.getMatriculeConseiller() + " déjà existant");
        }
    }

    public Conseiller findConsForCreate(Conseiller conseiller) {
        if (conseillers.findConseillerById(conseiller.getId()) != null)
            return null;
        return conseiller;
    }

    @SneakyThrows
    public List<Disponibilite> getAllDisponibilitesByConseiller(String conseillerCin) {
        Conseiller conseiller1 = conseillers.findConseillerById(new PersonneId(conseillerCin));
        if (conseiller1 == null)
            throw new ConseillerException("Conseiller avec le cin : " + conseillerCin + " inexistant");
        else {
            return conseiller1.getDisponibilites();
        }
    }

    @SneakyThrows
    public Conseiller createOrUpdateDisponibilite(String conseillerCin,String date) {
        List<Disponibilite> list = getAllDisponibilitesByConseiller(conseillerCin);
        String[] t = date.split("-");
        int day=Integer.parseInt(t[2]);
        int month=Integer.parseInt(t[1]);
        int year=Integer.parseInt(t[0]);
        LocalDate date1 = LocalDate.of(year,month,day);
        for (int i = 0; i < list.size(); i++) {
            if (day==list.get(i).getDate().getDayOfMonth() && month==list.get(i).getDate().getMonthValue() && year==list.get(i).getDate().getYear()){
                throw new ConseillerException("disponibilite avec la date : " + date + " existante pour ce conseiller");
            }
        }
        Conseiller conseiler =  disponibiliteService.createOrUpdateDisponibilite(conseillers.findConseillerById(new PersonneId(conseillerCin)), date1);
        conseillers.save(conseiler);
        return conseiler;
    }

    public Disponibilite getDisponibilite(String conseillerCin, String date) {
        List<Disponibilite> list = getAllDisponibilitesByConseiller(conseillerCin);
        String[] t = date.split("-");
        int day=Integer.parseInt(t[2]);
        int month=Integer.parseInt(t[1]);
        int year=Integer.parseInt(t[0]);
        for (int i = 0; i < list.size(); i++) {
            if (day==list.get(i).getDate().getDayOfMonth() && month==list.get(i).getDate().getMonthValue() && year==list.get(i).getDate().getYear()){
                return list.get(i);
            }
        }
        throw new ConseillerException("disponibilite avec la date : " + date + " inexistante pour ce conseiller");
    }

    private String getFuseauHoraireByPays(String pays) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Map<String,String> map = objectMapper.readValue(new FileInputStream("src\\service\\src\\main\\java\\ma\\bcp\\digital\\project\\service\\pays+fh.json"), Map.class);
            return map.get(pays);

        }catch(Exception e){
            e.getMessage();
        }
        return "GMT";

    }

    public List<Creneaux> getAllFreeDisponibilites(String iddossier) {
        Dossier dossier = dossierService.findDossierById(iddossier);
        String pays = dossier.getProspect().getPaysResidence();
        Conseiller conseiller = getConseillerByPays(pays);
        String fuseauHoraire = getFuseauHoraireByPays(pays);
        List<Disponibilite> disponibilites = conseiller.getDisponibilites();
        Disponibilite disp;
        List<Creneaux> crenauxx;
        List<Creneaux> cren =new ArrayList<>();
        Creneaux crenaux;
        int i;
        int j;
        int l=disponibilites.size();
        int l1;
        for(i=0;i<l;i++){
            disp = disponibilites.get(i);
            crenauxx = disp.getCreneaux();
            l1 = crenauxx.size();
            for(j=0;j<l1;j++){
                    crenaux = crenauxx.get(j);
                    crenaux = disponibiliteService.adapterCrenaux(crenaux,fuseauHoraire);
                    cren.add(crenaux);
            }
        }
        return cren;
    }
    private Conseiller getConseillerByPays(String pays) {
        if(pays.equals("Maroc") || pays.equals("Chypre") || pays.equals("Argentine")) return conseillers.findConseillerById(new PersonneId("AB1234"));
        else if (pays.equals("France") || pays.equals("Espagne")) return conseillers.findConseillerById(new PersonneId("AB1235"));
        else return conseillers.findConseillerById(new PersonneId("AB1236"));
    }

    public Conseiller findConseillerUsingIdDossier(String idDossier){
        Dossier dossier = dossierService.findDossierById(idDossier);
        String pays = dossier.getProspect().getPaysResidence();
        return this.getConseillerByPays(pays);
    }

    // Supposons qu'on remplit la table des disponibilités de façon non ordonnée sinon on prend le dernier élément
    public LocalDate getMaxDateDispoByCons(String cin){
        List<Disponibilite> disponibiliteList = this.getAllDisponibilitesByConseiller(cin);
        LocalDate dateMax = disponibiliteList.get(0).getDate();
        for(Disponibilite dispo : disponibiliteList){
            if(dateMax.compareTo(dispo.getDate()) < 0){
                dateMax = dispo.getDate();
            }
        }
        return dateMax;
    }

    public void deleteDispoForCons(String cin, Disponibilite disponibilite){
        Conseiller conseiller = this.findConseillerById(cin);
        conseiller.getDisponibilites().remove(disponibilite);
    }

    public Creneaux findCreneauByDateAndHeure(String cin, String date, String heureDebut){
        Disponibilite disponibilite = this.getDisponibilite(cin,date);
        return disponibiliteService.findCrenauxByHeureDebut(disponibilite, heureDebut);
    }

    public void supprimerCrenauxConseillerByHeure(String conseillerCin,String date, String heureDebut){
        Disponibilite disponibilite = this.getDisponibilite(conseillerCin, date);

        Creneaux cr = disponibiliteService.findCrenauxByHeureDebut(disponibilite,heureDebut);
        disponibilite.getCreneaux().remove(cr);
        disponibiliteService.deleteCrenaux(cr);

        if(disponibilite.getCreneaux() == null || disponibilite.getCreneaux().isEmpty()){
            this.deleteDispoForCons(conseillerCin,disponibilite);
            disponibiliteService.deleteDisponibilite(disponibilite);
            this.ajouterUneNouvelleDate(conseillerCin);
        }
    }

    public void ajouterUneNouvelleDate(String conseillerCin){
        LocalDate lastDate = this.getMaxDateDispoByCons(conseillerCin);
        if(lastDate.getDayOfWeek() == DayOfWeek.FRIDAY){
            this.createOrUpdateDisponibilite(conseillerCin,lastDate.plusDays(3).toString());
        }else{
            this.createOrUpdateDisponibilite(conseillerCin,lastDate.plusDays(1).toString());
        }
    }

}
