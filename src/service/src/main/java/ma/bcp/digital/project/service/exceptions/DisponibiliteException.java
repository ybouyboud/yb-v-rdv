package ma.bcp.digital.project.service.exceptions;

public class DisponibiliteException extends RuntimeException {
    public DisponibiliteException(String message) {
        super(message);
    }
}
