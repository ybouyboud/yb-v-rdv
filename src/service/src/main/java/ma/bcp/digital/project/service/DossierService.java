package ma.bcp.digital.project.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import ma.bcp.digital.project.domain.entities.Dossier;
import ma.bcp.digital.project.domain.entities.DossierId;
import ma.bcp.digital.project.domain.entities.PieceJustif;
import ma.bcp.digital.project.domain.entities.Prospect;
import ma.bcp.digital.project.domain.repositories.DossierRepository;
import ma.bcp.digital.project.domain.repositories.PieceJustifRepository;
import ma.bcp.digital.project.service.exceptions.DossierException;

import java.util.List;

@AllArgsConstructor
public class DossierService {

    private DossierRepository dossiers;
    private ProspectService prospectService;
    private PieceJustifRepository pieceJustifRepository;


    public List<Dossier> findAll() {
        if(dossiers.findAll().toString().equals("[]")) throw new DossierException("Pas de Dossier(s) à afficher.");
        return (List<Dossier>) dossiers.findAll();
    }

    public Dossier findDossierById(String dossierId){
        Dossier dossier = dossiers.findDossierById(new DossierId(dossierId));
        if(dossier == null) throw new DossierException("Dossier avec dossierId " + dossierId + " introuvable");
        return dossier;
    }

    public Dossier findDossierByReference(String reference){
        Dossier dossier = dossiers.findDossierByReference(reference.toUpperCase());
        if(dossier == null) throw new DossierException("Dossier avec REFERENCE : "+reference.toUpperCase()+" introuvable");
        return dossier;
    }


    @SneakyThrows
    public Dossier createOrUpdateDossier(Dossier dossier){

        List<PieceJustif> pieceJustifList = dossier.getPiecesJustif();
        Prospect p = dossier.getProspect();
        Dossier d = dossier;
        d.setProspect(null);

        if(!pieceJustifList.isEmpty()){
            d.setPiecesJustif(null);
            d.setId(dossier.getId());
            dossiers.save(d);
            for(PieceJustif pj : pieceJustifList){
                pj.setDossier(d);
                pieceJustifRepository.save(pj);
            }
            d.setPiecesJustif(pieceJustifList);
        }

        dossiers.save(d);
        p.setDossier(d);
        prospectService.createOrUpdateProspect(p);
        d.setProspect(p);
        d.setReference(d.getReference().toUpperCase());
        dossiers.save(d);
        return dossier;
    }

}
