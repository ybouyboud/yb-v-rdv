package ma.bcp.digital.project.service.exceptions;


public class PieceIdentiteException extends RuntimeException {
    public PieceIdentiteException(String message) {
        super(message);
    }
}
