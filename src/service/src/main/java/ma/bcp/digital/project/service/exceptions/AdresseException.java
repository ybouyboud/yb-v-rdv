package ma.bcp.digital.project.service.exceptions;

public class AdresseException extends RuntimeException {
    public AdresseException(String message) {
        super(message);
    }
}
