package ma.bcp.digital.project.service.exceptions;

public class DossierException extends RuntimeException {
    public DossierException(String message) {
        super(message);
    }
}
