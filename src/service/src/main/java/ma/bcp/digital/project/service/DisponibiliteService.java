package ma.bcp.digital.project.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import ma.bcp.digital.project.domain.entities.*;
import ma.bcp.digital.project.domain.repositories.CrenauxRepository;
import ma.bcp.digital.project.domain.repositories.DisponibiliteRepository;
import ma.bcp.digital.project.service.exceptions.DisponibiliteException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;

@AllArgsConstructor
public class DisponibiliteService {

    private DisponibiliteRepository disponibiliteRepository;
    private CrenauxRepository crenauxRepository;

    @SneakyThrows
    public Conseiller createOrUpdateDisponibilite(Conseiller conseiller, LocalDate date){
        if(LocalDate.now().isAfter(date)) throw new DisponibiliteException("Vous devez saisir une date à partir du "+LocalDate.now());
        List<Creneaux> creneaux = new ArrayList<>();
        Disponibilite disponibilite = new Disponibilite(date,creneaux,conseiller);
        disponibilite.setId(new DisponibiliteId());
        List<Disponibilite> disponibilites = conseiller.getDisponibilites();
        disponibiliteRepository.save(disponibilite);
        ajouterCreneaux(disponibilite);
        disponibiliteRepository.save(disponibilite);
        disponibilites.add(disponibilite);
        conseiller.setDisponibilites(disponibilites);
        return conseiller;
    }

    @SneakyThrows
    private void ajouterCreneaux(Disponibilite disponibilite) {
        LocalTime time = LocalTime.of(8,0);
        Creneaux crenaux;
        List<Creneaux> crenauxx = new ArrayList<>();
        int i;
        for(i=0;i<12;i++){
            crenaux = new Creneaux(time.toString(),time.plusMinutes(40).toString(),disponibilite);
            crenaux.setId(new CreneauxId());
            crenauxx.add(crenaux);
            crenauxRepository.save(crenaux);
            time=time.plusMinutes(40);
        }
        disponibilite.setCreneaux(crenauxx);
    }

    public Creneaux adapterCrenaux(Creneaux crenaux, String fuseauHoraire) {
        int i;
        String[] times = {crenaux.getTempsDebut(),crenaux.getTempsFin()};
        String[] heure;
        for(i=0;i<2;i++){
            heure = times[i].split(":");
            ZonedDateTime zdt = ZonedDateTime.of(crenaux.getDisponibilite().getDate(),LocalTime.of(Integer.parseInt(heure[0]),Integer.parseInt(heure[1])),ZoneId.of("GMT+1"));
            Calendar calendar = GregorianCalendar.from(zdt);
            DateFormat formatter = new SimpleDateFormat("dd MM yyyy HH:mm:ss z");
            formatter.setTimeZone(TimeZone.getTimeZone(fuseauHoraire));
            if(i==0) crenaux.setTempsDebut(formatter.format(calendar.getTime()));
            else crenaux.setTempsFin(formatter.format(calendar.getTime()));
        }
        return crenaux;
    }

    public Creneaux findCrenauxByHeureDebut(Disponibilite disponibilite, String heureDebut){
        for(Creneaux cr : disponibilite.getCreneaux()){
            if(cr.getTempsDebut().equals(heureDebut))
                return cr;
        }
        return null;
    }

    public void deleteCrenaux(Creneaux creneau){
        crenauxRepository.delete(creneau);
    }
    public void deleteDisponibilite(Disponibilite disponibilite){ disponibiliteRepository.delete(disponibilite);}





}
