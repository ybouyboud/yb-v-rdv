package ma.bcp.digital.project.web.adapter.rest;

import ma.bcp.digital.project.service.RdvService;
import ma.bcp.digital.project.web.models.output.RdvModel;
import ma.bcp.digital.project.web.rest.RdvsAPI;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/1/rdvs")
public class RdvsController extends RdvsAPI {
    public RdvsController(RdvService rdvService) {
        super(rdvService);
    }

    @GetMapping
    public List<RdvModel> listRdvs(){ return super.listAllRdvs();  }

    @GetMapping("/create/{date}/{heureDebut}/{dossierId}")
    public RdvModel createNewRdv(@PathVariable String dossierId, @PathVariable String date, @PathVariable String heureDebut){
        return super.createRdv(dossierId,date,heureDebut);
    }

    @GetMapping("/update/{date}/{heureDebut}/{dossierId}")
    public RdvModel updateRdvConseiller(@PathVariable String dossierId, @PathVariable String date, @PathVariable String heureDebut){
        return super.updateRdv(dossierId,date,heureDebut);
    }
}
