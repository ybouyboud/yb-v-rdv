package ma.bcp.digital.project.web.adapter.rest;

import ma.bcp.digital.project.domain.entities.Dossier;
import ma.bcp.digital.project.service.DossierService;
import ma.bcp.digital.project.web.models.output.DossierModel;
import ma.bcp.digital.project.web.rest.DossiersAPI;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/1/dossiers")
public class DossiersController extends DossiersAPI {

    public DossiersController(DossierService dossierService) {
        super(dossierService);
    }

    @GetMapping
    public List<DossierModel> listDossiers(){return super.listDossiers();}

    @GetMapping("/id/{dossierId}")
    public DossierModel listById(@PathVariable String dossierId){ return super.findDossierById(dossierId); }

    @GetMapping("reference/{reference}")
    public DossierModel findDossierByReference(@PathVariable String reference){
        return super.findDossierByReference(reference);
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public DossierModel createOrUpdateDossier(@Valid @RequestBody Dossier dossier) {
        return super.createOrUpdateDossier(dossier);
    }

}
