package ma.bcp.digital.project.web.adapter.rest;

import ma.bcp.digital.project.service.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler

    public final ResponseEntity<Object> handleConseillerException(ConseillerException ex, WebRequest request){
        ConseillerExceptionResponse exceptionResponse = new ConseillerExceptionResponse(ex.getMessage());
        return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleDossierException(DossierException ex, WebRequest request){
        DossierExceptionResponse exceptionResponse = new DossierExceptionResponse(ex.getMessage());
        return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handlePieceJustifException(PieceJustifException ex, WebRequest request){
        PieceJustifExceptionResponse exceptionResponse = new PieceJustifExceptionResponse(ex.getMessage());
        return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handlePieceIdentiteException(PieceIdentiteException ex, WebRequest request){
        PieceIdentiteExceptionResponse exceptionResponse = new PieceIdentiteExceptionResponse(ex.getMessage());
        return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleAdresseException(AdresseException ex, WebRequest request){
        AdresseExceptionResponse exceptionResponse = new AdresseExceptionResponse(ex.getMessage());
        return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleProductException(ProductException ex, WebRequest request){
        ProductExceptionResponse exceptionResponse = new ProductExceptionResponse(ex.getMessage());
        return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleProspectException(ProspectException ex, WebRequest request){
        ProspectExceptionResponse exceptionResponse = new ProspectExceptionResponse(ex.getMessage());
        return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleTypeCompteException(TypeCompteException ex, WebRequest request){
        TypeCompteExceptionResponse exceptionResponse = new TypeCompteExceptionResponse(ex.getMessage());
        return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleRdvException(RdvException ex, WebRequest request){
        RdvExceptionResponse exceptionResponse = new RdvExceptionResponse(ex.getMessage());
        return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleDisponibiliteException(DisponibiliteException ex, WebRequest request){
        DisponibiliteExceptionResponse exceptionResponse = new DisponibiliteExceptionResponse(ex.getMessage());
        return new ResponseEntity(exceptionResponse, HttpStatus.BAD_REQUEST);
    }

}
