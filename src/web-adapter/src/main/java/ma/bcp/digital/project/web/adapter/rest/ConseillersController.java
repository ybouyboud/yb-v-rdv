package ma.bcp.digital.project.web.adapter.rest;

import lombok.SneakyThrows;
import ma.bcp.digital.project.domain.entities.Conseiller;
import ma.bcp.digital.project.service.ConseillerService;
import ma.bcp.digital.project.web.models.output.ConseillerModel;
import ma.bcp.digital.project.web.models.output.CrenauxModel;
import ma.bcp.digital.project.web.models.output.DisponibiliteModel;
import ma.bcp.digital.project.web.rest.ConseillersAPI;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/api/1/conseillers")
public class ConseillersController extends ConseillersAPI {

    public ConseillersController(ConseillerService conseillerService) {
        super(conseillerService);
    }

    @GetMapping
    public List<ConseillerModel> listConseillers(){return super.listConseillers();}

    @GetMapping("/cin/{cin}")
    public ConseillerModel listConseillerByCin(@PathVariable String cin){
        return super.findConseillerById(cin);
    }

    @GetMapping("/matriculeConseiller/{matriculeConseiller}")
    public ConseillerModel listConseillerByMatriculeConseiller(@PathVariable String matriculeConseiller){
        return super.findConseillerByMatriculeConseiller(matriculeConseiller);
    }

    @PostMapping("/create")
    public ConseillerModel createOrUpdateConseiller(@Valid @RequestBody Conseiller conseiller){
        return super.createOrUpdateConseiller(conseiller);
    }

    @SneakyThrows
    @GetMapping("/getdisponibilite/{conseillerCin}/{date}")
    public DisponibiliteModel getDisponibilite(@PathVariable("conseillerCin") String conseillerCin, @PathVariable("date") String date){
        return super.getDisponibilite(conseillerCin,date);
    }

    @SneakyThrows
    @GetMapping("/adddisponibilite/{conseillerCin}/{date}")
    public ConseillerModel createOrUpdateDisponibilite(@PathVariable("conseillerCin") String conseillerCin, @PathVariable("date") String date){
        return super.addDisponibilite(conseillerCin,date);
    }

    @SneakyThrows
    @GetMapping("/getalldisponibilites/{conseillerCin}")
    public List<DisponibiliteModel> getDisponibilites(@PathVariable("conseillerCin") String conseillerCin){
        return super.getDisponibilitesByConseiller(conseillerCin);
    }

    @SneakyThrows
    @GetMapping("/getDisponibility/{iddossier}")
    public List<CrenauxModel> getFreeDisponibilites(@PathVariable("iddossier") String iddossier){
        return super.getDisponibility(iddossier);
    }

}
