package ma.bcp.digital.project.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import ma.bcp.digital.project.domain.entities.Conseiller;
import ma.bcp.digital.project.domain.entities.Dossier;
import ma.bcp.digital.project.domain.entities.DossierId;
import ma.bcp.digital.project.domain.entities.PersonneId;
import ma.bcp.digital.project.service.DossierService;
import ma.bcp.digital.project.web.adapter.rest.DossiersController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DossierControllerTest {
    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected DossiersController dossiersController;

    @MockBean
    protected DossierService dossierService;

    @Test
    public void dossiersListTest() throws Exception {
        List<Dossier> dossierList = new ArrayList<>();
        for(int i = 0; i < 2;i++){
            Dossier dossier = new Dossier();
            dossier.setId(new DossierId());
            dossierList.add(dossier);
        }
        given(dossierService.findAll()).willReturn(dossierList);
        mockMvc.perform(
            get("/api/1/dossiers")).andExpect(status().isOk());
    }


    @Test
    public void listByIdTest() throws Exception {
        Dossier dossier = new Dossier();
        dossier.setId(new DossierId("D001"));
        given(dossierService.findDossierById(BDDMockito.anyString())).willReturn(dossier);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/1/dossiers/id/D001")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("dossierId").value("D001"))
            .andDo(print());
    }

    @Test
    public void findDossierByReferenceTest() throws Exception{
        Dossier dossier = new Dossier();
        dossier.setId(new DossierId());
        dossier.setReference("R001");
        given(dossierService.findDossierByReference(BDDMockito.anyString())).willReturn(dossier);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/1/dossiers/reference/R001")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("reference").value("R001"))
            .andDo(print());
    }

    @Test
    public void createOrUpdateDossierTest() throws Exception{
        Dossier dossierObject = new Dossier();
        dossierObject.setId(new DossierId());
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE,false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(dossierObject);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/1/dossiers/create")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(requestJson))
            .andExpect(status().is2xxSuccessful());
    }
}
