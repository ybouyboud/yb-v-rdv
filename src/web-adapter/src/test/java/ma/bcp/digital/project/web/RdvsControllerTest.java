package ma.bcp.digital.project.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import ma.bcp.digital.project.domain.entities.*;
import ma.bcp.digital.project.service.RdvService;
import ma.bcp.digital.project.web.adapter.rest.RdvsController;
import org.apache.tomcat.jni.Local;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RdvsControllerTest {
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected RdvsController rdvsController;

    @MockBean
    protected RdvService rdvService;

    @Test
    public void testRdvsList() throws Exception {
        List<Rdv> rdvList = new ArrayList<>();
        Conseiller conseiller = new Conseiller();
        conseiller.setId(new PersonneId("AB1234"));
        for(int i = 0; i < 2;i++){
            Rdv rdv = new Rdv();
            Dossier dossier = new Dossier();
            dossier.setId(new DossierId());
            rdv.setId(new RdvId());
            rdv.setDossier(dossier);
            rdv.setConseiller(conseiller);
            rdvList.add(rdv);
        }
        given(rdvService.findAll()).willReturn(rdvList);
        mockMvc.perform(
            get("/api/1/rdvs")).andExpect(status().isOk());

    }

    @Test
    public void createNewRdvTest() throws Exception{
        Rdv rdvObject = new Rdv();
        Dossier dossier = new Dossier();
        dossier.setId(new DossierId("D001"));
        Conseiller conseiller = new Conseiller();
        conseiller.setId(new PersonneId("AB1234"));
        rdvObject.setId(new RdvId());
        rdvObject.setDossier(dossier);
        rdvObject.setConseiller(conseiller);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/1/rdvs/create/"+ LocalDate.now().toString()+"/11:20/D001")
            .contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void updateRdvConseillerTest() throws Exception{
        Rdv rdvObject = new Rdv();
        Dossier dossier = new Dossier();
        dossier.setId(new DossierId("D001"));
        Conseiller conseiller = new Conseiller();
        conseiller.setId(new PersonneId("AB1234"));
        rdvObject.setId(new RdvId());
        rdvObject.setDossier(dossier);
        rdvObject.setConseiller(conseiller);
        rdvObject.setDate(LocalDate.now());
        rdvObject.setHeureDebut("11:20");
        rdvObject.setHeureFin("12:00");
        rdvObject.setFuseauHoraire("GMT+1");
        mockMvc.perform(MockMvcRequestBuilders.get("/api/1/rdvs/update/"+ LocalDate.now().plusDays(2).toString()+"/08:00/D001")
            .contentType(MediaType.APPLICATION_JSON_UTF8))
            .andExpect(status().is2xxSuccessful())
            .andDo(print());
    }
}
