package ma.bcp.digital.project.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import ma.bcp.digital.project.domain.entities.Conseiller;
import ma.bcp.digital.project.domain.entities.PersonneId;
import ma.bcp.digital.project.service.ConseillerService;
import ma.bcp.digital.project.web.adapter.rest.ConseillersController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ConseillerControllerTest {
    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    protected ConseillersController conseillersController;

    @MockBean
    private ConseillerService conseillerService;

    @Test
    public void testConseillersList() throws Exception {
        Conseiller conseiller1 = new Conseiller();
        conseiller1.setId(new PersonneId());
        Conseiller conseiller2 = new Conseiller();
        conseiller2.setId(new PersonneId());
        List<Conseiller> conseillerList = new ArrayList<>();
        conseillerList.add(conseiller1);
        conseillerList.add(conseiller2);
        given(conseillerService.findAll()).willReturn(conseillerList);
        mockMvc.perform(
            get("/api/1/conseillers")).andExpect(status().isOk());

    }

    @Test
    public void listConseillerByCinTest() throws Exception{
        Conseiller returnValue = new Conseiller();
        returnValue.setId(new PersonneId("AB1234"));
        given(conseillerService.findConseillerById(BDDMockito.anyString())).willReturn(returnValue);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/1/conseillers/cin/AB1234")
        .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("cin").value("AB1234"))
            .andDo(print());
    }


    @Test
    public void listConseillerByMatriculeConseillerTest() throws Exception{
        Conseiller returnValue = new Conseiller("u123456",null);
        returnValue.setId(new PersonneId("AB1234"));
        given(conseillerService.findConseillerByMatriculeConseiller(BDDMockito.anyString())).willReturn(returnValue);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/1/conseillers/matriculeConseiller/u123456")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("matriculeConseiller").value("u123456"))
            .andDo(print());
    }

    @Test
    public void createOrUpdateConseillerTest() throws Exception{
        Conseiller consObject = new Conseiller("u123456",null);
        consObject.setId(new PersonneId("AB1234"));
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE,false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(consObject);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/1/conseillers/create")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
        .content(requestJson))
        .andExpect(status().is2xxSuccessful());
    }


    @Test
    public void createOrUpdateDisponibiliteTest() throws Exception {
        Conseiller consObject = new Conseiller("u123456",null);
        consObject.setId(new PersonneId("AB1234"));

        mockMvc.perform(MockMvcRequestBuilders.get("/api/1/conseillers/adddisponibilite/AB1234/"+LocalDate.now().toString())
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().is2xxSuccessful())
        .andDo(print());
    }

}
