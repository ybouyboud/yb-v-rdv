package ma.bcp.digital.project.service.adapter;

import ma.bcp.digital.project.domain.repositories.DossierRepository;
import ma.bcp.digital.project.domain.repositories.PieceJustifRepository;
import ma.bcp.digital.project.service.DossierService;
import ma.bcp.digital.project.service.ProspectService;
import org.springframework.stereotype.Service;

@Service
public class DossierServiceAdapter extends DossierService {
    public DossierServiceAdapter(DossierRepository dossiers, ProspectService prospectService, PieceJustifRepository pieceJustifRepository) {
        super(dossiers, prospectService, pieceJustifRepository);
    }
}
