package ma.bcp.digital.project.service.adapter;

import ma.bcp.digital.project.domain.repositories.UserRepository;
import ma.bcp.digital.project.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceAdapter extends UserService {

    public UserServiceAdapter(UserRepository users) {
        super(users);
    }
}
