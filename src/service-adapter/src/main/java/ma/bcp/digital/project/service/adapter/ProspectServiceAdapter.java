package ma.bcp.digital.project.service.adapter;

import ma.bcp.digital.project.domain.repositories.*;
import ma.bcp.digital.project.service.ProspectService;
import org.springframework.stereotype.Service;


@Service
public class ProspectServiceAdapter extends ProspectService {
    public ProspectServiceAdapter(ProspectRepository prospects, PieceIdentiteRepository pieceIdentiteRepository, AdresseRepository adresseRepository, ProductRepository productRepository, TypeCompteRepository typeCompteRepository) {
        super(prospects, pieceIdentiteRepository, adresseRepository, productRepository, typeCompteRepository);
    }
}
