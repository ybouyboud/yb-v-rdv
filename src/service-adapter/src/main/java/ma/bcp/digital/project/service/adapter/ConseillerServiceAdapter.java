package ma.bcp.digital.project.service.adapter;

import ma.bcp.digital.project.domain.repositories.ConseillerRepository;
import ma.bcp.digital.project.service.ConseillerService;
import ma.bcp.digital.project.service.DisponibiliteService;
import ma.bcp.digital.project.service.DossierService;
import org.springframework.stereotype.Service;

@Service
public class ConseillerServiceAdapter extends ConseillerService {
    public ConseillerServiceAdapter(ConseillerRepository conseillers, DisponibiliteService disponibiliteService, DossierService dossierService) {
        super(conseillers, disponibiliteService, dossierService);
    }
}
