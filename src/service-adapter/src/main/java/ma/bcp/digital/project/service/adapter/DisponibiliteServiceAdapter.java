package ma.bcp.digital.project.service.adapter;


import ma.bcp.digital.project.domain.repositories.CrenauxRepository;
import ma.bcp.digital.project.domain.repositories.DisponibiliteRepository;
import ma.bcp.digital.project.service.DisponibiliteService;
import org.springframework.stereotype.Service;

@Service
public class DisponibiliteServiceAdapter extends DisponibiliteService {
    public DisponibiliteServiceAdapter(DisponibiliteRepository disponibiliteRepository, CrenauxRepository crenauxRepository) {
        super(disponibiliteRepository, crenauxRepository);
    }
}
