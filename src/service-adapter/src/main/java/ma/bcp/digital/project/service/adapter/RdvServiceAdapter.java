package ma.bcp.digital.project.service.adapter;

import ma.bcp.digital.project.domain.repositories.RdvRepository;
import ma.bcp.digital.project.service.ConseillerService;
import ma.bcp.digital.project.service.DisponibiliteService;
import ma.bcp.digital.project.service.DossierService;
import ma.bcp.digital.project.service.RdvService;
import org.springframework.stereotype.Service;

@Service
public class RdvServiceAdapter extends RdvService {
    public RdvServiceAdapter(RdvRepository rdvRepository, DossierService dossierService, ConseillerService conseillerService, DisponibiliteService disponibiliteService) {
        super(rdvRepository, dossierService, conseillerService, disponibiliteService);
    }
}
