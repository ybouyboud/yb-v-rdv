package ma.bcp.digital.project.domain;

import ma.bcp.digital.project.domain.support.Schema;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SchemaTest {

    @Test
    public void testSchema() {
        Assertions.assertEquals(Schema.ADRESSES, "adresses");
        Assertions.assertEquals(Schema.CONSEILLERS, "conseillers");
        Assertions.assertEquals(Schema.PROSPECTS, "prospects");
        Assertions.assertEquals(Schema.RDVS, "rdvs");
        Assertions.assertEquals(Schema.DOSSIERS, "dossiers");
        Assertions.assertEquals(Schema.TYPES_COMPTE, "typesCompte");
        Assertions.assertEquals(Schema.PRODUCTS, "products");
        Assertions.assertEquals(Schema.PIECES_JUSTIFS, "piecesJustifs");
        Assertions.assertEquals(Schema.PIECES_IDENTITE, "piecesIdentite");
        Assertions.assertEquals(Schema.DISPONIBILITES, "disponibilites");
        Assertions.assertEquals(Schema.CRENEAUX, "creneaux");
    }

}
