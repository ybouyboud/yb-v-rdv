package ma.bcp.digital.project.domain;

import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;
import ma.bcp.digital.project.domain.entities.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EntitiesTest {

    //private static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Test
    public void testBeans() {
        Validator validator = ValidatorBuilder.create()
            .with(new GetterTester())
            .with(new SetterTester())
            .build();

        validator.validateRecursively(Adresse.class.getPackage().getName());
        validator.validateRecursively(Creneaux.class.getPackage().getName());
        validator.validateRecursively(Disponibilite.class.getPackage().getName());
        validator.validateRecursively(Dossier.class.getPackage().getName());
        validator.validateRecursively(PieceIdentite.class.getPackage().getName());
        validator.validateRecursively(PieceJustif.class.getPackage().getName());
        validator.validateRecursively(Product.class.getPackage().getName());
        validator.validateRecursively(Rdv.class.getPackage().getName());
        validator.validateRecursively(TypeCompte.class.getPackage().getName());

    }



    @Test
    public void testAdresse() {
        Assertions.assertTrue(new AdresseId().getValue().startsWith("adr_"));
    }

    @Test
    public void testCreneaux() { Assertions.assertTrue(new CreneauxId().getValue().startsWith("cren_"));}

    @Test
    public void testDisponibilite() {
        Assertions.assertTrue(new DisponibiliteId().getValue().startsWith("disp_"));
    }

    @Test
    public void testDossier() {
        Assertions.assertTrue(new DossierId().getValue().startsWith("dossier_"));
    }

    @Test
    public void testPieceIdentite() {
        Assertions.assertTrue(new PieceIdentiteId().getValue().startsWith("pi_"));
    }

    @Test
    public void testPieceJustif() {
        Assertions.assertTrue(new PieceJustifId().getValue().startsWith("pj_"));
    }

    @Test
    public void testProduct() { Assertions.assertTrue(new ProductId().getValue().startsWith("prod_")); }

    @Test
    public void testRdv() { Assertions.assertTrue(new RdvId().getValue().startsWith("rdv_")); }

    @Test
    public void testTypeCompte() { Assertions.assertTrue(new TypeCompteId().getValue().startsWith("tc_")); }
}
