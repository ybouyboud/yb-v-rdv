package ma.bcp.digital.project.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.bcp.digital.project.domain.support.BaseEntity;
import ma.bcp.digital.project.domain.support.Schema;

import javax.persistence.Entity;
import javax.persistence.Table;


@Table(name = Schema.ROLES)
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Role extends BaseEntity<RoleId> {

    private String name;

    private String description;

}
