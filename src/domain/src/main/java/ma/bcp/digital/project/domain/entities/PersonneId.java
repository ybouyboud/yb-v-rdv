package ma.bcp.digital.project.domain.entities;

import lombok.Value;
import ma.bcp.digital.project.domain.support.EntityId;
import ma.bcp.digital.project.domain.support.IDs;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Value
public class PersonneId implements EntityId<String> {

    @Column(name = "cin")
    private String cin;

    public PersonneId() {
        this.cin = IDs.generate(PREFIX);
    }

    public PersonneId(String cin) {
        this.cin=cin;
    }

    public static final String PREFIX = "pers_";

    @Override
    public String getValue() {
        return cin;
    }
}
