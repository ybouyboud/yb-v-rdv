package ma.bcp.digital.project.domain.entities;

import lombok.Data;
import lombok.Value;
import ma.bcp.digital.project.domain.support.EntityId;
import ma.bcp.digital.project.domain.support.IDs;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Value
public class DisponibiliteId implements EntityId<String>{

    @Column(name = "disponibiliteId")
    private String idDisponibilite;

    public DisponibiliteId() {
        this.idDisponibilite = IDs.generate(PREFIX);
    }

    public DisponibiliteId(String idDisponibilite) {
        this.idDisponibilite = idDisponibilite;
    }

    public static final String PREFIX = "disp_";

    @Override
    public String getValue() {
        return idDisponibilite;
    }
}
