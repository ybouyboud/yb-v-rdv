package ma.bcp.digital.project.domain.repositories;

import ma.bcp.digital.project.domain.entities.Conseiller;
import ma.bcp.digital.project.domain.entities.PersonneId;

public interface ConseillerRepository extends EntityRepository<Conseiller, PersonneId> {
    Conseiller findConseillerByMatriculeConseiller(String matriculeConseiller);
    Conseiller findConseillerById(PersonneId cin);
}
