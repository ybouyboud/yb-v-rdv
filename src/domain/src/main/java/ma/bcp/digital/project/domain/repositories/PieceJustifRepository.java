package ma.bcp.digital.project.domain.repositories;

import ma.bcp.digital.project.domain.entities.PieceJustif;
import ma.bcp.digital.project.domain.entities.PieceJustifId;


public interface PieceJustifRepository extends EntityRepository<PieceJustif, PieceJustifId> {
}
