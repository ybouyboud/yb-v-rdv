package ma.bcp.digital.project.domain.entities;

import lombok.Value;
import ma.bcp.digital.project.domain.support.EntityId;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.UUID;

@Embeddable
@Value
public class UserId implements EntityId<String> {

    @Column(name = "id")
    private String value;

    public static final String PREFIX = "usr_";

    public UserId() {
        this.value = PREFIX + UUID.randomUUID().toString();
    }

    @Override
    public String getValue() {
        return null;
    }
}
