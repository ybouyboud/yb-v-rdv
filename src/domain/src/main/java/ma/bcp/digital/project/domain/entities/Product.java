package ma.bcp.digital.project.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.bcp.digital.project.domain.support.BaseEntity;
import ma.bcp.digital.project.domain.support.Schema;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = Schema.PRODUCTS)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Product extends BaseEntity<ProductId> {

     private String codeProduit;
     private String libelleProduit;
     private String typeProduit;

}
