package ma.bcp.digital.project.domain.support;

public class Schema {

    public static final String USERS = "users";
    public static final String ROLES = "roles";
    public static final String ADRESSES = "adresses";
    public static final String CONSEILLERS = "conseillers";
    public static final String PROSPECTS = "prospects";
    public static final String RDVS = "rdvs";
    public static final String DOSSIERS = "dossiers";
    public static final String TYPES_COMPTE  = "typesCompte";
    public static final String PRODUCTS = "products";
    public static final String PIECES_JUSTIFS = "piecesJustifs";
    public static final String PIECES_IDENTITE = "piecesIdentite";
    public static final String DISPONIBILITES = "disponibilites";
    public static final String CRENEAUX = "creneaux";
}
