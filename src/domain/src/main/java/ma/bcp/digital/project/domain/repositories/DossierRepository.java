package ma.bcp.digital.project.domain.repositories;

import ma.bcp.digital.project.domain.entities.Dossier;
import ma.bcp.digital.project.domain.entities.DossierId;

public interface DossierRepository extends EntityRepository<Dossier, DossierId> {
    Dossier findDossierById(DossierId dossierId);
    Dossier findDossierByReference(String reference);
}
