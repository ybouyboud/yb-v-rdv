package ma.bcp.digital.project.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Value;
import ma.bcp.digital.project.domain.support.EntityId;
import ma.bcp.digital.project.domain.support.IDs;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Value
public class PieceIdentiteId implements EntityId<String> {

    @Column(name = "piece_id")
    @AttributeOverride(name = "id", column = @Column(name = "piece_id"))
    private String idPieceIdentite;

    public PieceIdentiteId() {
        this.idPieceIdentite = IDs.generate(PREFIX);
    }

    public PieceIdentiteId(String idPieceIdentite){ this.idPieceIdentite = idPieceIdentite;}

    public static final String PREFIX = "pi_";

    @Override
    public String getValue() {
        return idPieceIdentite;
    }
}
