package ma.bcp.digital.project.domain.repositories;

import ma.bcp.digital.project.domain.entities.PieceIdentite;
import ma.bcp.digital.project.domain.entities.PieceIdentiteId;


public interface PieceIdentiteRepository extends EntityRepository<PieceIdentite, PieceIdentiteId> {
}
