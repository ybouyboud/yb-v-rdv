package ma.bcp.digital.project.domain.repositories;

import ma.bcp.digital.project.domain.entities.PersonneId;
import ma.bcp.digital.project.domain.entities.Prospect;

public interface ProspectRepository extends EntityRepository<Prospect, PersonneId> {
    Prospect findProspectById(PersonneId prospectId);
}
