package ma.bcp.digital.project.domain.entities;

import lombok.Data;
import lombok.Value;
import ma.bcp.digital.project.domain.support.EntityId;
import ma.bcp.digital.project.domain.support.IDs;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Value
public class CreneauxId implements EntityId<String>{

    @Column(name = "creanuxId")
    private String crenauxId;

    public CreneauxId() {
        this.crenauxId = IDs.generate(PREFIX);
    }

    public CreneauxId(String crenauxId) {
        this.crenauxId = crenauxId;
    }

    public static final String PREFIX = "cren_";

    @Override
    public String getValue() {
        return crenauxId;
    }
}
