package ma.bcp.digital.project.domain.entities;

import lombok.Data;
import ma.bcp.digital.project.domain.support.EntityId;
import ma.bcp.digital.project.domain.support.IDs;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Data
public class RdvId implements EntityId<String> {

    @Column(name = "id")
    @AttributeOverride(name = "id", column = @Column(name = "id"))
    private String id = IDs.generate(PREFIX);

    public static final String PREFIX = "rdv_";

    @Override
    public String getValue() {
        return id;
    }


}
