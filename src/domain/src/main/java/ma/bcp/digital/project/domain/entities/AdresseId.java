package ma.bcp.digital.project.domain.entities;

import lombok.Data;
import lombok.Value;
import ma.bcp.digital.project.domain.support.EntityId;
import ma.bcp.digital.project.domain.support.IDs;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Value
public class AdresseId implements EntityId<String> {

    @Column(name = "adresseId")
    private String idAdresse;

    public AdresseId() {
        this.idAdresse = IDs.generate(PREFIX);
    }

    public AdresseId(String idAdresse) {
        this.idAdresse = idAdresse;
    }

    public static final String PREFIX = "adr_";

    @Override
    public String getValue() {
        return idAdresse;
    }
}
