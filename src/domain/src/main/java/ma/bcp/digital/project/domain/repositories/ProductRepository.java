package ma.bcp.digital.project.domain.repositories;


import ma.bcp.digital.project.domain.entities.Product;
import ma.bcp.digital.project.domain.entities.ProductId;

public interface ProductRepository extends EntityRepository<Product, ProductId>{
}
