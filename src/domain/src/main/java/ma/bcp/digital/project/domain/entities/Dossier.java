package ma.bcp.digital.project.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.bcp.digital.project.domain.support.BaseEntity;
import ma.bcp.digital.project.domain.support.Schema;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = Schema.DOSSIERS)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Dossier extends BaseEntity<DossierId> {

    @Column(unique = true)
    private String reference;

    @Column(updatable = false)
    private Date dateCreation;

    @OneToOne
    @JoinColumn(name = "cin", referencedColumnName = "cin")
    private Prospect prospect;

    @OneToMany
    private List<PieceJustif> piecesJustif;


    @PrePersist
    protected void onCreate(){
        this.dateCreation =  new Date();
    }

}
