package ma.bcp.digital.project.domain.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.bcp.digital.project.domain.support.BaseEntity;
import ma.bcp.digital.project.domain.support.Schema;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = Schema.DISPONIBILITES)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Disponibilite extends BaseEntity<DisponibiliteId> {


    @Column(updatable = false)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.PERSIST)
    private List<Creneaux> creneaux;

    @ManyToOne
    private Conseiller conseiller;

}
