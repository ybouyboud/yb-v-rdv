package ma.bcp.digital.project.domain.entities;

import lombok.Value;
import ma.bcp.digital.project.domain.support.EntityId;
import ma.bcp.digital.project.domain.support.IDs;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Value
public class DossierId implements EntityId<String> {

    @Column(name = "dossierId")
    private String idDossier;

    public DossierId() {
        this.idDossier = IDs.generate(PREFIX);
    }

    public DossierId(String idDossier) {
        this.idDossier = idDossier;
    }

    public static final String PREFIX = "dossier_";

    @Override
    public String getValue() {
        return idDossier;
    }
}
