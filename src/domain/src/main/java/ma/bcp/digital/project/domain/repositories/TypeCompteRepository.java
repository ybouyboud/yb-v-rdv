package ma.bcp.digital.project.domain.repositories;

import ma.bcp.digital.project.domain.entities.TypeCompte;
import ma.bcp.digital.project.domain.entities.TypeCompteId;

public interface TypeCompteRepository extends EntityRepository<TypeCompte, TypeCompteId> {
}
