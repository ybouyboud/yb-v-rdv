package ma.bcp.digital.project.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.bcp.digital.project.domain.support.Schema;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = Schema.PROSPECTS)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Prospect extends Personne {

    protected String usage;
    protected double revenuNetMensuel;
    protected String activite;
    protected String nomPere;
    protected String nomMere;

    @OneToOne
    @JoinColumn(name = "dossierId")
    protected Dossier dossier;

    @OneToMany
    @JoinColumn
    protected List<PieceIdentite> piecesIdentite;

    @ManyToMany
    @JoinColumn(name = "adresseId")
    protected List<Adresse> adresses;

    @OneToMany
    @JoinColumn(name ="productId")
    protected List<Product> produits;

    @OneToMany
    protected List<TypeCompte> typeComptes;

}
