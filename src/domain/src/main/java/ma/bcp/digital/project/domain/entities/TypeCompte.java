package ma.bcp.digital.project.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.bcp.digital.project.domain.support.BaseEntity;
import ma.bcp.digital.project.domain.support.Schema;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = Schema.TYPES_COMPTE)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TypeCompte extends BaseEntity<TypeCompteId> {

    protected String codeCompte;
    protected String numCompte;

}
