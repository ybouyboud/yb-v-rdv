package ma.bcp.digital.project.domain.support;

import java.util.UUID;

public class IDs {

    public static String generate(String prefix) {
        return prefix + UUID.randomUUID().toString().replace("-", "");
    }

}
