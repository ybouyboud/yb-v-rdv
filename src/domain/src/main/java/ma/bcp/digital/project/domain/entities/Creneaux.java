package ma.bcp.digital.project.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.bcp.digital.project.domain.support.BaseEntity;
import ma.bcp.digital.project.domain.support.Schema;

import javax.persistence.*;

@Entity
@Table(name = Schema.CRENEAUX)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Creneaux extends BaseEntity<CreneauxId> {

    @Column(updatable = false)
    private String tempsDebut;

    @Column(updatable = false)
    private String tempsFin;

    @ManyToOne
    private Disponibilite disponibilite;

}
