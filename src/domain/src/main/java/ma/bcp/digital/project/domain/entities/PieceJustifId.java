package ma.bcp.digital.project.domain.entities;

import lombok.Value;
import ma.bcp.digital.project.domain.support.EntityId;
import ma.bcp.digital.project.domain.support.IDs;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Value
public class PieceJustifId implements EntityId<String> {

    @Column(name = "pieceJustifId")
    private String idPieceJustif;

    public PieceJustifId() {
        this.idPieceJustif = IDs.generate(PREFIX);
    }

    public PieceJustifId(String idPieceJustif) {
        this.idPieceJustif = idPieceJustif;
    }

    public static final String PREFIX = "pj_";

    @Override
    public String getValue() {
        return idPieceJustif;
    }
}
