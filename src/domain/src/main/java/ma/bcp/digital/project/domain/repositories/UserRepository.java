package ma.bcp.digital.project.domain.repositories;

import ma.bcp.digital.project.domain.entities.User;
import ma.bcp.digital.project.domain.entities.UserId;

public interface UserRepository extends EntityRepository<User, UserId> {

}
