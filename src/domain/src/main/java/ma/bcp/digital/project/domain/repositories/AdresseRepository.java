package ma.bcp.digital.project.domain.repositories;

import ma.bcp.digital.project.domain.entities.Adresse;
import ma.bcp.digital.project.domain.entities.AdresseId;

public interface AdresseRepository extends EntityRepository<Adresse, AdresseId> {

}
