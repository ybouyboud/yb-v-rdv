package ma.bcp.digital.project.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.bcp.digital.project.domain.support.Schema;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@Table(name = Schema.CONSEILLERS)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Conseiller extends Personne{

    @Column(unique = true, updatable = false)
    @NotBlank(message = "Champ matriculeConseiller requis")
    protected String matriculeConseiller;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.PERSIST)
    protected List<Disponibilite> disponibilites;

}
