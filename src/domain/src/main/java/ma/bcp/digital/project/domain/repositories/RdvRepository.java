package ma.bcp.digital.project.domain.repositories;

import ma.bcp.digital.project.domain.entities.Dossier;
import ma.bcp.digital.project.domain.entities.Rdv;
import ma.bcp.digital.project.domain.entities.RdvId;

public interface RdvRepository extends EntityRepository<Rdv, RdvId> {
    Rdv findRdvByDossier(Dossier dossier);
}
