package ma.bcp.digital.project.domain.repositories;

import ma.bcp.digital.project.domain.entities.Disponibilite;
import ma.bcp.digital.project.domain.entities.DisponibiliteId;


public interface DisponibiliteRepository extends EntityRepository<Disponibilite, DisponibiliteId>  {
    @Override
    void deleteById(DisponibiliteId disponibiliteId);
}
