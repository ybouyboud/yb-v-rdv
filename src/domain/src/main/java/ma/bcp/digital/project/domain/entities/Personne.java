package ma.bcp.digital.project.domain.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.bcp.digital.project.domain.support.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import java.io.Serializable;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)

public abstract class Personne extends BaseEntity<PersonneId> implements Serializable {

    protected String civilite;
    protected String nom;
    protected String prenom;
    @JsonFormat(pattern = "yyyy-MM-dd")
    protected Date dateNaissance;
    protected String paysNaissance;
    protected String lieuNaissance;
    protected String email;
    protected String tel1;
    protected String tel2;
    protected String paysResidence;
    protected String nationalite;
    protected String autreNationalite;

}
