package ma.bcp.digital.project.domain.repositories;

import ma.bcp.digital.project.domain.entities.Creneaux;
import ma.bcp.digital.project.domain.entities.CreneauxId;

public interface CrenauxRepository extends EntityRepository<Creneaux, CreneauxId>  {
}
