package ma.bcp.digital.project.domain.entities;

import lombok.Value;
import ma.bcp.digital.project.domain.support.EntityId;
import ma.bcp.digital.project.domain.support.IDs;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Value
public class RoleId implements EntityId<String> {

    @Column(name = "id")
    private String value = IDs.generate(PREFIX);

    public static final String PREFIX = "rol_";

    @Override
    public String getValue() {
        return value;
    }
}
