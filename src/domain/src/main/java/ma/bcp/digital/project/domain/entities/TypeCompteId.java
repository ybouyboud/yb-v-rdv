package ma.bcp.digital.project.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import ma.bcp.digital.project.domain.support.EntityId;
import ma.bcp.digital.project.domain.support.IDs;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Data
public class TypeCompteId implements EntityId<String> {

    @Column(name = "typeCompte_Id")
    private String idTypeCompte;

    public TypeCompteId() { this.idTypeCompte = IDs.generate(PREFIX); }

    public TypeCompteId(String idTypeCompte) {
        this.idTypeCompte = idTypeCompte;
    }

    public static final String PREFIX = "tc_";

    @Override
    public String getValue() {
        return idTypeCompte;
    }
}
