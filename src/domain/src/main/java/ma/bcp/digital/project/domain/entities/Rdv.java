package ma.bcp.digital.project.domain.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.bcp.digital.project.domain.support.BaseEntity;
import ma.bcp.digital.project.domain.support.Schema;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = Schema.RDVS)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Rdv extends BaseEntity<RdvId>{

    @ManyToOne
    @JoinColumn(name = "conseiller_cin", nullable = false)
    protected Conseiller conseiller;
    @OneToOne
    @JoinColumn(name = "dossierId", nullable = false, unique = true)
    protected Dossier dossier ;

    @JsonFormat(pattern = "yyyy-MM-dd")
    protected LocalDate date;
    protected String heureDebut;
    protected String heureFin;
    protected String fuseauHoraire;

}
