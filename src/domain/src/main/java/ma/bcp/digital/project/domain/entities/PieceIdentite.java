package ma.bcp.digital.project.domain.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.bcp.digital.project.domain.support.BaseEntity;
import ma.bcp.digital.project.domain.support.Schema;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = Schema.PIECES_IDENTITE)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PieceIdentite extends BaseEntity<PieceIdentiteId> {

    protected String typePiece;
    @Column(unique = true, updatable = false)
    protected String numPiece;
    protected String lieuDelivrance;
    @JsonFormat(pattern = "yyyy-MM-dd")
    protected Date dateDelivrance;
    @JsonFormat(pattern = "yyyy-MM-dd")
    protected Date dateExpiration;

    @ManyToOne
    @JoinColumn(name = "prospect_cin")
    protected Prospect prospect;

}
