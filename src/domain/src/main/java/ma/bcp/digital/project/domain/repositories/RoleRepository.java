package ma.bcp.digital.project.domain.repositories;

import ma.bcp.digital.project.domain.entities.Role;
import ma.bcp.digital.project.domain.entities.RoleId;

public interface RoleRepository extends EntityRepository<Role, RoleId> {

}
