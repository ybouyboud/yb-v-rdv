package ma.bcp.digital.project.domain.entities;

import lombok.Data;
import ma.bcp.digital.project.domain.support.EntityId;
import ma.bcp.digital.project.domain.support.IDs;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Data
public class ProductId implements EntityId<String> {

    @Column(name = "produit_id")
    @AttributeOverride(name = "id", column = @Column(name = "produit_id"))
    private String produitId;

    public ProductId() { this.produitId = IDs.generate(PREFIX); }

    public ProductId(String produitId) {
        this.produitId = produitId;
    }

    public static final String PREFIX = "prod_";

    @Override
    public String getValue() {
        return produitId;
    }
}
