package ma.bcp.digital.project.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.bcp.digital.project.domain.support.BaseEntity;
import ma.bcp.digital.project.domain.support.Schema;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = Schema.ADRESSES)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Adresse extends BaseEntity<AdresseId> {

    private String typeAdresse;
    private String adresse1;
    private String adresse2;
    private String codeVille;
    private String codePostal;
    private String codePays;

}
