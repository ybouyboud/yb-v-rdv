package ma.bcp.digital.project.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ma.bcp.digital.project.domain.support.BaseEntity;
import ma.bcp.digital.project.domain.support.Schema;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = Schema.PIECES_JUSTIFS)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PieceJustif extends BaseEntity<PieceJustifId> {

     protected String typeDocument;
     protected String pathAgent;
     protected String pathClient;

     @ManyToOne
    protected Dossier dossier;

}
