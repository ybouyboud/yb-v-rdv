package ma.bcp.digital.project.domain.adapter.repositories;

import ma.bcp.digital.project.domain.entities.TypeCompte;
import ma.bcp.digital.project.domain.entities.TypeCompteId;
import ma.bcp.digital.project.domain.repositories.TypeCompteRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TypeCompteJpaRepository extends TypeCompteRepository, JpaRepository<TypeCompte, TypeCompteId> {
}
