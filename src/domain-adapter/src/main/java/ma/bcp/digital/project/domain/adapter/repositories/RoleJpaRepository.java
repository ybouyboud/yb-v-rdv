package ma.bcp.digital.project.domain.adapter.repositories;

import ma.bcp.digital.project.domain.entities.Role;
import ma.bcp.digital.project.domain.entities.RoleId;
import ma.bcp.digital.project.domain.repositories.RoleRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleJpaRepository extends RoleRepository, JpaRepository<Role, RoleId> {

}



