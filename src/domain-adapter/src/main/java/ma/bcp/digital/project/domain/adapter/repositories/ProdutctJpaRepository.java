package ma.bcp.digital.project.domain.adapter.repositories;

import ma.bcp.digital.project.domain.entities.Product;
import ma.bcp.digital.project.domain.entities.ProductId;
import ma.bcp.digital.project.domain.repositories.ProductRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProdutctJpaRepository extends ProductRepository, JpaRepository<Product, ProductId> {
}
