package ma.bcp.digital.project.domain.adapter.repositories;

import ma.bcp.digital.project.domain.entities.User;
import ma.bcp.digital.project.domain.entities.UserId;
import ma.bcp.digital.project.domain.repositories.UserRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserJpaRepository extends UserRepository, JpaRepository<User, UserId> {

}



