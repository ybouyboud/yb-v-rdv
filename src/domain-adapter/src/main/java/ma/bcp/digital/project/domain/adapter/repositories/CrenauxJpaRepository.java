package ma.bcp.digital.project.domain.adapter.repositories;

import ma.bcp.digital.project.domain.entities.Creneaux;
import ma.bcp.digital.project.domain.entities.CreneauxId;
import ma.bcp.digital.project.domain.repositories.CrenauxRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CrenauxJpaRepository extends CrenauxRepository, JpaRepository<Creneaux, CreneauxId> {
}
