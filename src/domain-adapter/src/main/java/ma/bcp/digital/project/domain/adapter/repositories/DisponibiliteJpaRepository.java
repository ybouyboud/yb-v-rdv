package ma.bcp.digital.project.domain.adapter.repositories;

import ma.bcp.digital.project.domain.entities.Disponibilite;
import ma.bcp.digital.project.domain.entities.DisponibiliteId;
import ma.bcp.digital.project.domain.repositories.DisponibiliteRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DisponibiliteJpaRepository extends DisponibiliteRepository, JpaRepository<Disponibilite, DisponibiliteId> {
}
