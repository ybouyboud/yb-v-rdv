package ma.bcp.digital.project.domain.adapter.repositories;

import ma.bcp.digital.project.domain.entities.PersonneId;
import ma.bcp.digital.project.domain.entities.Prospect;
import ma.bcp.digital.project.domain.repositories.ProspectRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProspectJpaRepository extends ProspectRepository, JpaRepository<Prospect, PersonneId> {
}
