package ma.bcp.digital.project.domain.adapter.repositories;

import ma.bcp.digital.project.domain.entities.PieceJustif;
import ma.bcp.digital.project.domain.entities.PieceJustifId;
import ma.bcp.digital.project.domain.repositories.PieceJustifRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PieceJustifJpaRepository extends PieceJustifRepository, JpaRepository<PieceJustif, PieceJustifId>  {
}
