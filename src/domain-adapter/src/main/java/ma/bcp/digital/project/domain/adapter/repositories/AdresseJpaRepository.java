package ma.bcp.digital.project.domain.adapter.repositories;

import ma.bcp.digital.project.domain.entities.Adresse;
import ma.bcp.digital.project.domain.entities.AdresseId;
import ma.bcp.digital.project.domain.repositories.AdresseRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdresseJpaRepository extends AdresseRepository, JpaRepository<Adresse, AdresseId> {
}
