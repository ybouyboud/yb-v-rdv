package ma.bcp.digital.project.domain.adapter.repositories;

import ma.bcp.digital.project.domain.entities.PieceIdentite;
import ma.bcp.digital.project.domain.entities.PieceIdentiteId;
import ma.bcp.digital.project.domain.repositories.PieceIdentiteRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PieceIdentiteJpaRepository extends PieceIdentiteRepository, JpaRepository<PieceIdentite, PieceIdentiteId> {
}
