package ma.bcp.digital.project.domain.adapter.repositories;

import ma.bcp.digital.project.domain.entities.Rdv;
import ma.bcp.digital.project.domain.entities.RdvId;
import ma.bcp.digital.project.domain.repositories.RdvRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RdvJpaRepository extends RdvRepository, JpaRepository<Rdv, RdvId> {

}
