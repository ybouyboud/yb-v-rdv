package ma.bcp.digital.project.domain.adapter.repositories;

import ma.bcp.digital.project.domain.entities.Dossier;
import ma.bcp.digital.project.domain.entities.DossierId;
import ma.bcp.digital.project.domain.repositories.DossierRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DossierJpaRepository extends DossierRepository, JpaRepository<Dossier, DossierId> {
}
