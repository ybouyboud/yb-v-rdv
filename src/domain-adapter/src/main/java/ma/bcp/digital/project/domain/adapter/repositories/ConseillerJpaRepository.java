package ma.bcp.digital.project.domain.adapter.repositories;

import ma.bcp.digital.project.domain.entities.Conseiller;
import ma.bcp.digital.project.domain.entities.PersonneId;
import ma.bcp.digital.project.domain.repositories.ConseillerRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConseillerJpaRepository extends ConseillerRepository, JpaRepository<Conseiller, PersonneId> {

}
