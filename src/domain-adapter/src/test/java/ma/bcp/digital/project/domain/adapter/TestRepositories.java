package ma.bcp.digital.project.domain.adapter;

import ma.bcp.digital.project.domain.entities.PieceIdentite;
import ma.bcp.digital.project.domain.entities.TypeCompte;
import ma.bcp.digital.project.domain.repositories.*;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestRepositories {

    @Autowired
    private AdresseRepository adresses;
    @Autowired
    private ConseillerRepository conseillers;
    @Autowired
    private CrenauxRepository creneaux;
    @Autowired
    private DisponibiliteRepository disponibilites;
    @Autowired
    private DossierRepository dossiers;
    @Autowired
    private PieceIdentiteRepository pieceIdentites;
    @Autowired
    private PieceJustifRepository pieceJustifs;
    @Autowired
    private ProductRepository products;
    @Autowired
    private ProspectRepository prospects;
    @Autowired
    private RdvRepository rdvs;
    @Autowired
    private TypeCompteRepository typeComptes;

    @Test
    public void testAdresses() {
        Assertions.assertNotNull(adresses);
        Assertions.assertEquals(0, adresses.count());
    }

    @Test
    public void testConseillers() {
        Assertions.assertNotNull(conseillers);
        Assertions.assertEquals(0, conseillers.count());
    }

    @Test
    public void testCreneaux() {
        Assertions.assertNotNull(creneaux);
        Assertions.assertEquals(0, creneaux.count());
    }

    @Test
    public void testDisponibilites() {
        Assertions.assertNotNull(disponibilites);
        Assertions.assertEquals(0, disponibilites.count());
    }

    @Test
    public void testDossiers() {
        Assertions.assertNotNull(dossiers);
        Assertions.assertEquals(0, dossiers.count());
    }

    @Test
    public void testPieceIdentites() {
        Assertions.assertNotNull(pieceIdentites);
        Assertions.assertEquals(0, pieceIdentites.count());
    }

    @Test
    public void testPieceJustifs() {
        Assertions.assertNotNull(pieceJustifs);
        Assertions.assertEquals(0, pieceJustifs.count());
    }

    @Test
    public void testProducts() {
        Assertions.assertNotNull(products);
        Assertions.assertEquals(0, products.count());
    }

    @Test
    public void testRdvs() {
        Assertions.assertNotNull(rdvs);
        Assertions.assertEquals(0, rdvs.count());
    }

    @Test
    public void testTypeComptes() {
        Assertions.assertNotNull(typeComptes);
        Assertions.assertEquals(0, typeComptes.count());
    }

}
