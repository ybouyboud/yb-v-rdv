package ma.bcp.digital.project.web.models.output;

import lombok.Value;

@Value
public class UserModel {

    private String id;
    private String name;
    private String picture;
    private RoleModel role;

}
