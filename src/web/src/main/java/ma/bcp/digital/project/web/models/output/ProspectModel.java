package ma.bcp.digital.project.web.models.output;

import lombok.Value;

import java.util.Date;
import java.util.List;

@Value
public class ProspectModel{
    private String cin;
    private String civilite;
    private String nom;
    private String prenom;
    private Date dateNaissance;
    private String paysNaissance;
    private String lieuNaissance;
    private String email;
    private String tel1;
    private String tel2;
    private String paysResidence;
    private String nationalite;
    private String autreNationalite;
    private String usage;
    private double revenuNetMensuel;
    private String activite;
    private String nomPere;
    private String nomMere;
    private String dossierId;
    private List<PieceIdentiteModel> pieceIdentites;
    private List<AdresseModel> adresses;
    private List<ProductModel> products;
    private List<TypeCompteModel> typeComptes;


}
