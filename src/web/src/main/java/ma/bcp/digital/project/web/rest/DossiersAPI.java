package ma.bcp.digital.project.web.rest;

import lombok.AllArgsConstructor;
import ma.bcp.digital.project.domain.entities.Dossier;
import ma.bcp.digital.project.service.DossierService;
import ma.bcp.digital.project.web.models.factories.DossierFactory;
import ma.bcp.digital.project.web.models.output.DossierModel;

import java.util.List;

@AllArgsConstructor
public class DossiersAPI {
    private DossierService dossierService;


    public List<DossierModel> listDossiers(){
        return DossierFactory.transform(dossierService.findAll());}

    public DossierModel findDossierById(String dossierId){
        return DossierFactory.transform(dossierService.findDossierById(dossierId));
    }

    public DossierModel findDossierByReference(String reference){
        return DossierFactory.transform(dossierService.findDossierByReference(reference));
    }

    public DossierModel createOrUpdateDossier(Dossier dossier){
        return DossierFactory.transform(dossierService.createOrUpdateDossier(dossier));
    }

}
