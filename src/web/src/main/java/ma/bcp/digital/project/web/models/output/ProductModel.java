package ma.bcp.digital.project.web.models.output;

import lombok.Value;

@Value
public class ProductModel {

    private String produitId;
    private String codeProduit;
    private String libelleProduit;
    private String typeProduit;
}
