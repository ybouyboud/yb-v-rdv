package ma.bcp.digital.project.web.models.output;

import lombok.Value;

import java.time.LocalDate;


@Value
public class RdvModel {

    private String idRdv;
    private LocalDate date;
    private String heureDebut;
    private String heureFin;
    private String fuseauHoraire;
    private String dossierId;
    private String conseillerCin;


}
