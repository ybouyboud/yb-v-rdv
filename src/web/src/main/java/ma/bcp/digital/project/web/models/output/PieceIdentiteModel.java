package ma.bcp.digital.project.web.models.output;

import lombok.Value;

import java.util.Date;

@Value
public class PieceIdentiteModel {
    protected String pieceIdentiteId;
    protected String typePiece;
    protected String numPiece;
    protected String lieuDelivrance;
    protected Date dateDelivrance;
    protected Date dateExpiration;
    protected String prospectId;
}
