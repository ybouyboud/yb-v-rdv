package ma.bcp.digital.project.web.models.output;

import lombok.Value;

import java.util.Date;
import java.util.List;

@Value
public class DossierModel {
    private String dossierId;
    private String reference;
    private Date dateCreation;
    private ProspectModel prospect;
    private List<PieceJustifModel> piecesJustif;
}
