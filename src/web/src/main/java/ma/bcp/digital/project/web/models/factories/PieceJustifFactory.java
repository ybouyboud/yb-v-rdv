package ma.bcp.digital.project.web.models.factories;

import ma.bcp.digital.project.domain.entities.PieceJustif;
import ma.bcp.digital.project.web.models.output.PieceJustifModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PieceJustifFactory {
    public static List<PieceJustifModel> transform(List<PieceJustif> pjs){
        if(pjs == null) return new ArrayList<>();
        return pjs.stream().map(pieceJustif -> new PieceJustifModel(
            pieceJustif.getId().getValue(),
            pieceJustif.getTypeDocument(),
            pieceJustif.getPathAgent(),
            pieceJustif.getPathClient(),
            pieceJustif.getDossier().getId().getValue()
        )).collect(Collectors.toList());
    }

    public static PieceJustifModel transform(PieceJustif pieceJustif){
        if(pieceJustif == null) return null;
        return new PieceJustifModel(
            pieceJustif.getId().getValue(),
            pieceJustif.getTypeDocument(),
            pieceJustif.getPathAgent(),
            pieceJustif.getPathClient(),
            pieceJustif.getDossier().getId().getValue()

        );
    }
}
