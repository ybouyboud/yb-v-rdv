package ma.bcp.digital.project.web.models.factories;

import ma.bcp.digital.project.domain.entities.PieceIdentite;
import ma.bcp.digital.project.web.models.output.PieceIdentiteModel;

import java.util.List;
import java.util.stream.Collectors;

public class PieceIdentiteFactory {
    public static List<PieceIdentiteModel> transform(List<PieceIdentite> pieceIdentites) {
        return pieceIdentites.stream().map(pieceIdentite -> new PieceIdentiteModel(
            pieceIdentite.getId().getValue(),
            pieceIdentite.getTypePiece(),
            pieceIdentite.getNumPiece(),
            pieceIdentite.getLieuDelivrance(),
            pieceIdentite.getDateDelivrance(),
            pieceIdentite.getDateExpiration(),
            pieceIdentite.getProspect().getId().getCin()
        )).collect(Collectors.toList());
    }

    public static PieceIdentiteModel transform(PieceIdentite pieceIdentite) {
        if (pieceIdentite == null) return null;
        return new PieceIdentiteModel(
            pieceIdentite.getId().getValue(),
            pieceIdentite.getTypePiece(),
            pieceIdentite.getNumPiece(),
            pieceIdentite.getLieuDelivrance(),
            pieceIdentite.getDateDelivrance(),
            pieceIdentite.getDateExpiration(),
            pieceIdentite.getProspect().getId().getCin()
        );
    }
}

