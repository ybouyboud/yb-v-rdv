package ma.bcp.digital.project.web.models.factories;


import ma.bcp.digital.project.domain.entities.Prospect;
import ma.bcp.digital.project.web.models.output.ProspectModel;

import java.util.List;
import java.util.stream.Collectors;

public class ProspectFactory {

    public static List<ProspectModel> transform(List<Prospect> prospects) {
        return prospects.stream().map(prospect -> new ProspectModel(
            prospect.getId().getValue(),
            prospect.getCivilite(),
            prospect.getNom(),
            prospect.getPrenom(),
            prospect.getDateNaissance(),
            prospect.getPaysNaissance(),
            prospect.getLieuNaissance(),
            prospect.getEmail(),
            prospect.getTel1(),
            prospect.getTel2(),
            prospect.getPaysResidence(),
            prospect.getNationalite(),
            prospect.getAutreNationalite(),
            prospect.getUsage(),
            prospect.getRevenuNetMensuel(),
            prospect.getActivite(),
            prospect.getNomPere(),
            prospect.getNomMere(),
            prospect.getDossier().getId().getValue(),
            PieceIdentiteFactory.transform(prospect.getPiecesIdentite()),
            AdresseFactory.transform(prospect.getAdresses()),
            ProductFactory.transform(prospect.getProduits()),
            TypeCompteFactory.transform(prospect.getTypeComptes())
        )).collect(Collectors.toList());

    }

    public static ProspectModel transform(Prospect prospect){
        if(prospect == null) return null;
        return new ProspectModel(
            prospect.getId().getValue(),
            prospect.getCivilite(),
            prospect.getNom(),
            prospect.getPrenom(),
            prospect.getDateNaissance(),
            prospect.getPaysNaissance(),
            prospect.getLieuNaissance(),
            prospect.getEmail(),
            prospect.getTel1(),
            prospect.getTel2(),
            prospect.getPaysResidence(),
            prospect.getNationalite(),
            prospect.getAutreNationalite(),
            prospect.getUsage(),
            prospect.getRevenuNetMensuel(),
            prospect.getActivite(),
            prospect.getNomPere(),
            prospect.getNomMere(),
            prospect.getDossier().getId().getValue(),
            PieceIdentiteFactory.transform(prospect.getPiecesIdentite()),
            AdresseFactory.transform(prospect.getAdresses()),
            ProductFactory.transform(prospect.getProduits()),
            TypeCompteFactory.transform(prospect.getTypeComptes())
            );
    }

}
