package ma.bcp.digital.project.web.models.output;

import lombok.Value;


@Value
public class CrenauxModel {

    private String crenauxId;
    private String tempsDebut;
    private String tempsFin;
    private String conseillerNom;
}
