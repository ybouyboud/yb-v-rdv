package ma.bcp.digital.project.web.models.output;

import lombok.Value;

@Value
public class TypeCompteModel {

    protected String typeCompteId;
    protected String codeCompte;
    protected String numCompte;
}
