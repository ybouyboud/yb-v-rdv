package ma.bcp.digital.project.web.models.output;

import lombok.Value;

@Value
public class RoleModel {

    private String id;
    private String name;
    private String description;

}
