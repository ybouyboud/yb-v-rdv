package ma.bcp.digital.project.web.models.factories;

import ma.bcp.digital.project.domain.entities.Dossier;
import ma.bcp.digital.project.web.models.output.DossierModel;

import java.util.List;
import java.util.stream.Collectors;

public class DossierFactory {
    public static List<DossierModel> transform(List<Dossier> dossiers){

        return dossiers.stream().map(dossier -> new DossierModel(
            dossier.getId().getValue(),
            dossier.getReference(),
            dossier.getDateCreation(),
            ProspectFactory.transform(dossier.getProspect()),
            PieceJustifFactory.transform(dossier.getPiecesJustif())
        )).collect(Collectors.toList());
    }

    public static DossierModel transform(Dossier dossier) {

        if (dossier == null) return null;
        return new DossierModel(
            dossier.getId().getValue(),
            dossier.getReference(),
            dossier.getDateCreation(),
            ProspectFactory.transform(dossier.getProspect()),
            PieceJustifFactory.transform(dossier.getPiecesJustif())

        );
    }
}
