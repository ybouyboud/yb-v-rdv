package ma.bcp.digital.project.web.models.factories;

import ma.bcp.digital.project.domain.entities.Adresse;
import ma.bcp.digital.project.web.models.output.AdresseModel;

import java.util.List;
import java.util.stream.Collectors;

public class AdresseFactory  {

    public static AdresseModel transform(Adresse adresse) {

        if (adresse == null) return null;
        return new AdresseModel(
            adresse.getId().getValue(),
            adresse.getTypeAdresse(),
            adresse.getAdresse1(),
            adresse.getAdresse2(),
            adresse.getCodeVille(),
            adresse.getCodePostal(),
            adresse.getCodePays()
        );

    }

    public static List<AdresseModel> transform(List<Adresse> adresses){
        return adresses.stream().map(adresse  -> new AdresseModel(
            adresse.getId().getValue(),
            adresse.getTypeAdresse(),
            adresse.getAdresse1(),
            adresse.getAdresse2(),
            adresse.getCodeVille(),
            adresse.getCodePostal(),
            adresse.getCodePays()
        )).collect(Collectors.toList());
    }
}
