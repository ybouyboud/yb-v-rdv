package ma.bcp.digital.project.web.models.factories;

import ma.bcp.digital.project.domain.entities.Disponibilite;
import ma.bcp.digital.project.web.models.output.DisponibiliteModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DisponibiliteFactory {

    public static List<DisponibiliteModel> transform(List<Disponibilite> disponibilites){
        if (disponibilites==null) return new ArrayList<>();
        else{
            return disponibilites.stream().map(disponibilite -> new DisponibiliteModel(
                disponibilite.getId().getValue(),
                disponibilite.getDate(),
                disponibilite.getConseiller().getPrenom()+" "+disponibilite.getConseiller().getNom(),
                CrenauxFactory.transform(disponibilite.getCreneaux())
            )).collect(Collectors.toList());
        }

    }

    public static DisponibiliteModel transform(Disponibilite disponibilite) {

        if (disponibilite == null) return null;
        return new DisponibiliteModel(
            disponibilite.getId().getValue(),
            disponibilite.getDate(),
            disponibilite.getConseiller().getPrenom()+" "+disponibilite.getConseiller().getNom(),
            CrenauxFactory.transform(disponibilite.getCreneaux())

        );
    }
}
