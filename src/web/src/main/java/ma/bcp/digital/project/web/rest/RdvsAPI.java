package ma.bcp.digital.project.web.rest;

import lombok.AllArgsConstructor;
import ma.bcp.digital.project.service.RdvService;
import ma.bcp.digital.project.web.models.factories.RdvFactory;
import ma.bcp.digital.project.web.models.output.RdvModel;

import java.util.List;

@AllArgsConstructor
public class RdvsAPI {
    private RdvService rdvService;

    public List<RdvModel> listAllRdvs(){
        return RdvFactory.transform(rdvService.findAll());
    }

    public RdvModel createRdv(String dossierId, String date, String heureDebut){
        return RdvFactory.transform(rdvService.createRdv(dossierId,date,heureDebut));
    }

    public RdvModel updateRdv(String dossierId, String date, String heureDebut){
        return RdvFactory.transform(rdvService.updateRdv(dossierId,date,heureDebut));
    }

}
