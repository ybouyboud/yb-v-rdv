package ma.bcp.digital.project.web.models.factories;

import ma.bcp.digital.project.domain.entities.Rdv;
import ma.bcp.digital.project.web.models.output.RdvModel;

import java.util.List;
import java.util.stream.Collectors;

public class RdvFactory {
    public static List<RdvModel> transform(List<Rdv> rdvs){

        return rdvs.stream().map(rdv -> new RdvModel(
            rdv.getId().getValue(),
            rdv.getDate(),
            rdv.getHeureDebut(),
            rdv.getHeureFin(),
            rdv.getFuseauHoraire(),
            rdv.getDossier().getId().getValue(),
            rdv.getConseiller().getId().getValue()
        )).collect(Collectors.toList());
    }

    public static RdvModel transform(Rdv rdv) {

        if (rdv == null) return null;
        return new RdvModel(
            rdv.getId().getValue(),
            rdv.getDate(),
            rdv.getHeureDebut(),
            rdv.getHeureFin(),
            rdv.getFuseauHoraire(),
            rdv.getDossier().getId().getValue(),
            rdv.getConseiller().getId().getValue()
        );
    }
}
