package ma.bcp.digital.project.web.models.factories;

import ma.bcp.digital.project.domain.entities.Product;
import ma.bcp.digital.project.web.models.output.ProductModel;

import java.util.List;
import java.util.stream.Collectors;

public class ProductFactory {

    public static List<ProductModel> transform(List<Product> products) {
        return products.stream().map(product -> new ProductModel(
            product.getId().getValue(),
            product.getCodeProduit(),
            product.getLibelleProduit(),
            product.getTypeProduit()
        )).collect(Collectors.toList());
    }


    public static ProductModel transform(Product product) {
        if(product == null) return null;
        return new ProductModel(
            product.getId().getValue(),
            product.getCodeProduit(),
            product.getLibelleProduit(),
            product.getTypeProduit()
        );
    }
}
