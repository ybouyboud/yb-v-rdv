package ma.bcp.digital.project.web.models.output;

import lombok.Value;

import java.time.LocalDate;
import java.util.List;

@Value
public class DisponibiliteModel {

    private String disponibiliteId;
    private LocalDate date;
    private String conseillerNom;
    private List<CrenauxModel> crenaux;

}
