package ma.bcp.digital.project.web.models.output;

import lombok.Value;

import java.util.Date;
import java.util.List;

@Value
public class ConseillerModel{

    protected String cin;
    protected String matriculeConseiller;
    protected String civilite;
    protected String nom;
    protected String prenom;
    protected Date dateNaissance;
    protected String paysNaissance;
    protected String lieuNaissance;
    protected String email;
    protected String tel1;
    protected String tel2;
    protected String paysResidence;
    protected String nationalite;
    protected String autreNationalite;
    protected List<DisponibiliteModel> disponibiltes;


}
