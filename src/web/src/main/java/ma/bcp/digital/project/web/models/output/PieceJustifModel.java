package ma.bcp.digital.project.web.models.output;

import lombok.Value;

@Value
public class PieceJustifModel {
    private String pieceJustifId;
    private String typeDocument;
    private String pathAgent;
    private String pathClient;
    private String dossierId;
}
