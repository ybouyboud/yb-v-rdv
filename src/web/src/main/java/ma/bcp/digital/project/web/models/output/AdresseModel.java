package ma.bcp.digital.project.web.models.output;


import lombok.Value;

@Value
public class AdresseModel {

    private String adresseId;
    private String typeAdresse;
    private String adresse1;
    private String adresse2;
    private String codeVille;
    private String codePostal;
    private String codePays;
}
