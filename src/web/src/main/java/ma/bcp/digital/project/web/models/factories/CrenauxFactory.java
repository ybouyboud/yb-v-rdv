package ma.bcp.digital.project.web.models.factories;



import ma.bcp.digital.project.domain.entities.Creneaux;
import ma.bcp.digital.project.web.models.output.CrenauxModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CrenauxFactory {
    public static List<CrenauxModel> transform(List<Creneaux> crenauxx){
        if (crenauxx==null) return new ArrayList<>();
        else{
            return crenauxx.stream().map(crenaux -> new CrenauxModel(
                crenaux.getId().getValue(),
                crenaux.getTempsDebut(),
                crenaux.getTempsFin(),
                crenaux.getDisponibilite().getConseiller().getPrenom()+" "+crenaux.getDisponibilite().getConseiller().getNom()
            )).collect(Collectors.toList());
        }
    }

    public static CrenauxModel transform(Creneaux crenaux) {

        if (crenaux == null) return null;
        return new CrenauxModel(
            crenaux.getId().getValue(),
            crenaux.getTempsDebut(),
            crenaux.getTempsFin(),
            crenaux.getDisponibilite().getConseiller().getPrenom()+" "+crenaux.getDisponibilite().getConseiller().getNom()

        );
    }
}
