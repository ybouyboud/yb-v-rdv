package ma.bcp.digital.project.web.rest;

import lombok.AllArgsConstructor;
import ma.bcp.digital.project.domain.entities.Conseiller;
import ma.bcp.digital.project.service.ConseillerService;
import ma.bcp.digital.project.web.models.factories.ConseillerFactory;
import ma.bcp.digital.project.web.models.factories.CrenauxFactory;
import ma.bcp.digital.project.web.models.factories.DisponibiliteFactory;
import ma.bcp.digital.project.web.models.output.ConseillerModel;
import ma.bcp.digital.project.web.models.output.CrenauxModel;
import ma.bcp.digital.project.web.models.output.DisponibiliteModel;

import java.util.List;

@AllArgsConstructor
public class ConseillersAPI {
    private ConseillerService conseillerService;


    public List<ConseillerModel> listConseillers(){
        return ConseillerFactory.transform(conseillerService.findAll());}

    public ConseillerModel findConseillerById(String cin){
        return ConseillerFactory.transform(conseillerService.findConseillerById(cin));
    }

    public ConseillerModel findConseillerByMatriculeConseiller(String matriculeConseiller){
        return ConseillerFactory.transform(conseillerService.findConseillerByMatriculeConseiller(matriculeConseiller));
    }

    public ConseillerModel createOrUpdateConseiller(Conseiller conseiller){
        return ConseillerFactory.transform(conseillerService.createOrUpdateConseiller(conseiller));
    }

    public ConseillerModel addDisponibilite(String conseillerCin,String date){
        return ConseillerFactory.transform(conseillerService.createOrUpdateDisponibilite(conseillerCin,date));
    }

    protected DisponibiliteModel getDisponibilite(String conseillerCin, String date) {
        return DisponibiliteFactory.transform(conseillerService.getDisponibilite(conseillerCin,date));
    }

    protected List<DisponibiliteModel> getDisponibilitesByConseiller(String conseillerCin) {
        return DisponibiliteFactory.transform(conseillerService.getAllDisponibilitesByConseiller(conseillerCin));
    }

    protected List<CrenauxModel> getDisponibility(String iddossier) {
        return CrenauxFactory.transform(conseillerService.getAllFreeDisponibilites(iddossier));
    }
}
