package ma.bcp.digital.project.web.models.factories;


import ma.bcp.digital.project.domain.entities.Conseiller;
import ma.bcp.digital.project.web.models.output.ConseillerModel;

import java.util.List;
import java.util.stream.Collectors;

public class ConseillerFactory {
    public static List<ConseillerModel> transform(List<Conseiller> conseillers){
        return conseillers.stream().map(conseiller -> new ConseillerModel(
            conseiller.getId().getValue(),
            conseiller.getMatriculeConseiller(),
            conseiller.getCivilite(),
            conseiller.getNom(),
            conseiller.getPrenom(),
            conseiller.getDateNaissance(),
            conseiller.getPaysNaissance(),
            conseiller.getLieuNaissance(),
            conseiller.getEmail(),
            conseiller.getTel1(),
            conseiller.getTel2(),
            conseiller.getPaysResidence(),
            conseiller.getNationalite(),
            conseiller.getAutreNationalite(),
            DisponibiliteFactory.transform(conseiller.getDisponibilites())
            )).collect(Collectors.toList());
    }

    public static ConseillerModel transform(Conseiller conseiller){
        if(conseiller == null) return null;
        return new ConseillerModel(
            conseiller.getId().getValue(),
            conseiller.getMatriculeConseiller(),
            conseiller.getCivilite(),
            conseiller.getNom(),
            conseiller.getPrenom(),
            conseiller.getDateNaissance(),
            conseiller.getPaysNaissance(),
            conseiller.getLieuNaissance(),
            conseiller.getEmail(),
            conseiller.getTel1(),
            conseiller.getTel2(),
            conseiller.getPaysResidence(),
            conseiller.getNationalite(),
            conseiller.getAutreNationalite(),
            DisponibiliteFactory.transform(conseiller.getDisponibilites())
        );
    }
}
