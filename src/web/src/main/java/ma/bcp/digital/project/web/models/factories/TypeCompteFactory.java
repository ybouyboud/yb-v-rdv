package ma.bcp.digital.project.web.models.factories;

import ma.bcp.digital.project.domain.entities.TypeCompte;
import ma.bcp.digital.project.web.models.output.TypeCompteModel;

import java.util.List;
import java.util.stream.Collectors;

public class TypeCompteFactory {

    public static List<TypeCompteModel> transform(List<TypeCompte> typeComptes) {
        return typeComptes.stream().map(typeCompte -> new TypeCompteModel(
            typeCompte.getId().getValue(),
            typeCompte.getCodeCompte(),
            typeCompte.getNumCompte()
        )).collect(Collectors.toList());
    }

    public static TypeCompteModel transform(TypeCompte typeCompte) {
        if(typeCompte == null) return null;
        return new TypeCompteModel(
            typeCompte.getId().getValue(),
            typeCompte.getCodeCompte(),
            typeCompte.getNumCompte()
        );
    }
}
