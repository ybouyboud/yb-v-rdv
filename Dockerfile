FROM amazoncorretto:8
VOLUME /tmp
COPY src/web-adapter/build/libs/web.jar app.jar

ENV SPRING_PROFILES_ACTIVE "production"

EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-jar","/app.jar"]

